-- Init
registerForEvent('onInit', function()
    -- PlayerSystem
    local PlayerSystem = Game.GetPlayerSystem()
    if not PlayerSystem then return end

    -- Player
    local Player = PlayerSystem:GetLocalPlayerMainGameObject()
    if not Player then return end

    -- StatSystem
    local StatSystem = Game.GetStatsSystem()
    if not StatSystem then return end

    HolsterStarted = false
    HolsterTimer = 0.0
    UntrackStarted = false
    UntrackTimer = 0.0
    WalkToggle = false
    WalkHold = false
    WalkEffect = Game['gameRPGManager::CreateStatModifier;gamedataStatTypegameStatModifierTypeFloat']('MaxSpeed', 'Multiplier', 0.4)
end)

-- Walk
function WalkEffect_apply()
    local Player        = Game.GetPlayer()
    local StatSystem    = Game.GetStatsSystem()

    StatSystem:AddModifier(Player:GetEntityID(), WalkEffect)
end
function WalkEffect_remove()
    local Player        = Game.GetPlayer()
    local StatSystem    = Game.GetStatsSystem()

    StatSystem:RemoveModifier(Player:GetEntityID(), WalkEffect)
end
registerHotkey('controls_overhaul_walk_toggle', 'Toggle Walk', function()
    WalkEffect_remove()
    WalkHold = false

    if not WalkToggle then
      WalkEffect_apply()
      WalkToggle = true
    else
      WalkToggle = false
    end
end)
registerInput('controls_overhaul_walk_hold', 'Hold Walk', function()
    WalkEffect_remove()
    WalkToggle = false

    if not WalkHold then
      WalkEffect_apply()
      WalkHold = true
    else
      WalkHold = false
    end
end)

-- Holster
registerInput('controls_overhaul_holster', 'Hide Weapon', function()

    local times = Game.GetTimeSystem()
    gameTimeNew = times:GetGameTimeStamp()

    if HolsterStarted then
        HolsterStarted = false
        holdDuration = gameTimeNew - HolsterTimer
        HolsterTimer = gameTimeNew

        print("Holster hotkey released", holdDuration)
    else
        HolsterStarted = true
        print("Holster hotkey pressed")

        HolsterTimer = gameTimeNew
        return
    end

    -- approximately 4 seconds
    if holdDuration > 12.0 and holdDuration < 22.0 then
        local GetQuickSlotsManager = Game.GetPlayer():GetQuickSlotsManager()
        local GetTransaction = Game.GetTransactionSystem()
        local GetEquipment = Game.GetScriptableSystemsContainer():Get('EquipmentSystem')
        local GetPlayerEquipmentData = GetEquipment:GetPlayerData(Game.GetPlayer())

        local HasWeapon = GetTransaction:GetItemInSlot(Game.GetPlayer(), TweakDBID.new("AttachmentSlots.WeaponRight"))

        if HasWeapon then
            GetQuickSlotsManager:HideWeapon()
        else
            -- GetQuickSlotsManager:RequestWeaponEquip(GetPlayerEquipmentData:GetActiveWeapon())
        end
    end

end)

-- Untrack Quest
registerInput('controls_overhaul_untrack', 'Untrack Quest', function()

    local times = Game.GetTimeSystem()
    gameTimeNew = times:GetGameTimeStamp()

    if UntrackStarted then
        UntrackStarted = false
        holdDuration = gameTimeNew - UntrackTimer
        UntrackTimer = gameTimeNew
        print("Untrack hotkey released", holdDuration)
    else
        UntrackStarted = true
        print("Untrack hotkey pressed")

        UntrackTimer = gameTimeNew
        return
    end

    -- approximately 3 seconds
    if holdDuration > 10.0 and holdDuration < 20.0 then
        Game.untrack()
        local blackboard = Game.GetBlackboardSystem():Get(Game.GetAllBlackboardDefs().UI_System)
        local uiSystemBB = (Game.GetAllBlackboardDefs().UI_System)
        blackboard:SignalBool(uiSystemBB.IsInMenu)
    end

end)
