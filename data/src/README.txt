v1.3.3

- walk key removed
- CET hotkeys for walking, holstering and quest untracking

===========================================================


 tip: E = Enter, Q = RMB = Escape (almost always)
╭---------------┬-----------------------------------------╮
| key           | action                                  |
├---------------┼-----------------------------------------┤
| Backspace     | call vehicle                            |
| Tab           | weapon wheel                            |
| Q             | cycle quest obj. / open notification    |
| Q / RMB       | quit or cancel anything                 |
| E             | primary action                          |
| E*            | fast travel                             |
| F             | quick melee attack                      |
| F* / G*       | prev/next filter by waypoint type       |
| G             | grenade                                 |
| L*            | legend                                  |
| H             | hub menu                                |
| J*            | open related quest                      |
| LShift        | run hold                                |
| Z             | medkit                                  |
| X             | secondary action                        |
| C             | cyberware                               |
| V             | vision hold                             |
| V*            | center at V                             |
| Alt           | dodge key (+ direction, simultaneously) |
| Space* / MMB* | set waypoint                            |
| Space**       | sell/craft/disassemble all items        |
| MMB**         | sell/craft/disassemble 1/2 items        |
| MMB           | vision toggle                           |
| Mouse wheel** | increase/decrease quantity              |
╰---------------┴-----------------------------------------╯
 * only at the map view
 ** only with the item quantity picker widget

 You must enable "Dodging on Movement Input Keys"
 toggle in CONTROLS sections / reset to defaults
 in order for dodging to work. Double-tapping the
 keys is fully disabled.
