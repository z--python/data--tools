# Data tools

## Reusing tools in your project

1. `.gitlab-ci.yml`:
```
include: 'https://gitlab.com/mikko-timofeev/python_data_tools/raw/main/.gitlab-ci.yml'

variables:
  PACKAGE_VERSION: 1.2.3  # <- your desired version here
  ZIP_NAME: editedData-ci_build  # <- your desired artifact name here
```

2. `data/src/`
Place your sources here

3. `data/mod/`
Place your edits here
