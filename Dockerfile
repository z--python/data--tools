FROM bitnami/python:3.9

RUN install_packages zip libxml2-utils

WORKDIR /app
COPY ./core/requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt
