#!/bin/sh

# available mods:
# <same_name> - original file
# N_append - just add elements to parents' end
# N_prepend - just add elements to parents' beginning
# N_remove - remove elements
# N_replace - overwrite or add elements
#   * N represents order of operation (sorted alphabetically)

rm -r ./data/out_min/* 2> /dev/null
rm -r ./data/out/* 2> /dev/null
rm -r ./data/tmp/* 2> /dev/null

find ./data/src \
  -not -path '*/\.*' \
  -type f -print0 | xargs -0 -I{} \
    echo > ./data/tmp/manifest_src.txt \
    {}
cut -c12- ./data/tmp/manifest_src.txt >> ./data/tmp/manifest.txt

while read x_file_relpath; do

  x_file_dir=$(dirname "$x_file_relpath")
  mkdir -p "./data/tmp/$x_file_dir"

  # prepare output dirs as well
  mkdir -p "./data/out/$x_file_dir"
  mkdir -p "./data/out_min/$x_file_dir"

  cp "./data/src/$x_file_relpath" "./data/tmp/${x_file_relpath}"

  for mod in $(find "./data/mod/$x_file_relpath" -type f ! -name '.*' 2> /dev/null); do
    python3 ./core/libs/edit.py "./data/tmp/${x_file_relpath}" $mod \
      > "./data/tmp/${x_file_relpath}.tmp"
    mv "./data/tmp/${x_file_relpath}.tmp" "./data/tmp/${x_file_relpath}"
  done

  mv "./data/tmp/${x_file_relpath}" "./data/out_min/${x_file_relpath}"
done < ./data/tmp/manifest.txt

# prepare petty printed versions
while read some_file; do
  case $some_file in
    *.xml)
      xmllint "./data/out_min/$some_file" --format \
        > "./data/out/$some_file"
    ;;
    *.json)
      python -m json.tool "./data/out_min/$some_file" \
        > "./data/out/$some_file"
    ;;
    *)
      cp "./data/out_min/$some_file" \
        "./data/out/$some_file"
    ;;
  esac
done < ./data/tmp/manifest.txt

# create zip files

if [ -z "$1" ] && [ -z "$ZIP_NAME" ]; then
  echo "Output ready"
else
  if [ -z "$ZIP_NAME" ]; then
    ZIP_NAME=$1
  fi

  if [ -z "$PACKAGE_VERSION" ]; then
    PACKAGE_VERSION=$2
  fi

  cd ./data/out_min
  zip -r ./$ZIP_NAME-$PACKAGE_VERSION-min.zip *

  cd ../out
  zip -r ./$ZIP_NAME-$PACKAGE_VERSION.zip *

  cd ../..
fi

ls -l ./data/out_min
ls -l ./data/out
