#! /usr/bin/python3
# -*- coding: utf-8 -*-


import json
from io import StringIO
from copy import copy, deepcopy
from ruamel.yaml import YAML
from sys import argv as args
from xmltodict import parse as xml_parse, unparse as xml_dump


def deep_merge(target, src):
    for k, v in src.items():
        if type(v) == list:
            if not k in target:
                target[k] = deepcopy(v)
            else:
                target[k].extend(v)
        elif type(v) == dict:
            if not k in target:
                target[k] = deepcopy(v)
            else:
                deep_merge(target[k], v)
        elif type(v) == set:
            if not k in target:
                target[k] = v.copy()
            else:
                target[k].update(v.copy())
        else:
            target[k] = copy(v)
    return target


class Data:
    def __init__(self, filename):
        self.tool = filename.split(".")[-1]
        file = open(filename, "r")
        self.data_raw = file.read()
        file.close()
        self.data_dict = self.parse()

    def parse(self):
        default = lambda: YAML().load(self.data_raw)
        return {
            "json": lambda: json.loads(self.data_raw),
            "xml": lambda: xml_parse(self.data_raw),
            "yaml": default,
        }.get(self.tool, default)()

    def dump(self):
        string_stream = StringIO()
        default = lambda: YAML().dump(self.data_dict, string_stream)
        {
            "json": lambda: json.dump(self.data_dict, string_stream),
            "xml": lambda: xml_dump(self.data_dict, string_stream),
            "yaml": default,
        }.get(self.tool, default)()
        output_str = string_stream.getvalue()
        string_stream.close()
        return output_str

    def append(self, datasource_two):
        # TODO
        pass

    def prepend(self, datasource_two):
        # TODO
        pass

    def remove(self, datasource_two):
        # TODO
        pass

    def replace(self, datasource_two):
        # TODO: allow rootless data map (with unnamed root element)
        root = list(self.data_dict.keys())[0]
        self.data_dict[root] = deep_merge(
            self.data_dict[root], datasource_two.data_dict[root]
        )


def data_manage(filename, filemod):
    data1, data2 = Data(filename), Data(filemod)

    modtype = filemod[filemod.index("_") + 1 : filemod.rfind(".")]
    default = lambda: data1.append(data2)
    {
        "append": default,
        "prepend": lambda: data1.prepend(data2),
        "remove": lambda: data1.remove(data2),
        "replace": lambda: data1.replace(data2),
    }.get(modtype, default)()
    return data1.dump()


def main():
    result = data_manage(
        args[1],  # file to change
        args[2],  # list of changes
    )

    print(result)


if __name__ == "__main__":
    main()
